#!/usr/bin/env perl

#==============================================================================
# datefmt.t
# File ID: cbc683c8-a45e-11ea-8d29-4f45262dc9b5
#
# Test suite for datefmt(1).
#
# Character set: UTF-8
# ©opyleft 2020– Øyvind A. Holm <sunny@sunbase.org>
# License: GNU General Public License version 2 or later, see end of file for 
# legal stuff.
#==============================================================================

use strict;
use warnings;

BEGIN {
	use Test::More qw{no_plan};
	# use_ok() goes here
}

use Getopt::Long;

local $| = 1;

our $CMDB = "datefmt";
our $CMD = "../$CMDB";

our %Opt = (

	'all' => 0,
	'help' => 0,
	'quiet' => 0,
	'todo' => 0,
	'verbose' => 0,
	'version' => 0,

);

our $progname = $0;
$progname =~ s/^.*\/(.*?)$/$1/;
our $VERSION = '0.0.0';

my %descriptions = ();

Getopt::Long::Configure('bundling');
GetOptions(

	'all|a' => \$Opt{'all'},
	'help|h' => \$Opt{'help'},
	'quiet|q+' => \$Opt{'quiet'},
	'todo|t' => \$Opt{'todo'},
	'verbose|v+' => \$Opt{'verbose'},
	'version' => \$Opt{'version'},

) || die("$progname: Option error. Use -h for help.\n");

$Opt{'verbose'} -= $Opt{'quiet'};
$Opt{'help'} && usage(0);
if ($Opt{'version'}) {
	print_version();
	exit(0);
}

exit(main());

sub main {
	my $Retval = 0;

	diag(sprintf('========== Executing %s v%s ==========',
	             $progname, $VERSION));

	if ($Opt{'todo'} && !$Opt{'all'}) {
		goto todo_section;
	}

	test_standard_options();
	test_executable();

	todo_section:
	;

	if ($Opt{'all'} || $Opt{'todo'}) {
		diag('Running TODO tests...');
		TODO: {
			local $TODO = '';
			# Insert TODO tests here.
		}
	}

	diag('Testing finished.');

	return $Retval;
}

sub test_executable {
	my %s = (

		0 => '0s',
		1 => '1s',
		5 => '5s',
		10 => '10s',
		59 => '59s',
		60 => '1m:00s',
		61 => '1m:01s',
		119 => '1m:59s',
		120 => '2m:00s',
		121 => '2m:01s',
		3599 => '59m:59s',
		3600 => '1h:00m:00s',
		3601 => '1h:00m:01s',
		65536 => '18h:12m:16s',
		86399 => '23h:59m:59s',
		86400 => '1d:00h:00m:00s',
		86401 => '1d:00h:00m:01s',
		86460 => '1d:00h:01m:00s',
		864000 => '10d:00h:00m:00s',
		8640000 => '100d:00h:00m:00s',
		31556951 => '365d:05h:49m:11s', # 86400 * 365.2425 - 1
		31556952 => '1y:0d:00h:00m:00s', # 86400 * 365.2425
		31643352 => '1y:1d:00h:00m:00s', # 86400 * 366.2425
		31740797 => '1y:2d:03h:04m:05s', # 86400 * (365.2425 + 2 + 3/24 + 4/1440 + 5/86400)
		315569519 => '9y:365d:05h:49m:11s', # 86400 * 10 * 365.2425 - 1
		315569520 => '10y:0d:00h:00m:00s', # 86400 * 10 * 365.2425
		3155695200 => '100y:0d:00h:00m:00s', # 86400 * 100 * 365.2425
		31556952000 => '1000y:0d:00h:00m:00s', # 86400 * 1000 * 365.2425
		38946142857 => '1234y:56d:07h:08m:09s', # 86400 * (1234 * 365.2425 + 56 + 7/24 + 8/1440 + 9/86400)
		31556951999999 => '999999y:365d:05h:49m:11s', # 86400 * 1000000 * 365.2425 - 1
		31556952000000 => '1000000y:0d:00h:00m:00s', # 86400 * 1000000 * 365.2425
		31556952000001 => '1000000y:0d:00h:00m:01s', # 86400 * 1000000 * 365.2425 + 1
		31556952065536 => '1000000y:0d:18h:12m:16s', # 86400 * 1000000 * 365.2425 + 65536
		31556952086400 => '1000000y:1d:00h:00m:00s', # 86400 * (1000000 * 365.2425 + 1)
		31556960596801 => '1000000y:99d:12h:00m:01s', # 86400 * (1000000 * 365.2425 + 99 + 12/24 + 1/86400)
		315569520000000 => '10000000y:0d:00h:00m:00s', # 86400 * 10000000 * 365.2425

	);
	for my $f (sort { $a <=> $b } keys %s) {
		checkval($f, $s{$f});
	}
	checkval("''", "");
	checkval("nonumber", "nonumber");
	testcmd("echo nonumber | $CMD",
		"nonumber\n", "", 0,
		"\"nonumber\"");
	testcmd("echo 9999 with text | $CMD",
		"2h:46m:39s with text\n", "", 0,
		"\"9999 with text\"");
}

sub checkval {
	my ($in, $out) = @_;
	diag("$in => \"$out\"");
	testcmd("$CMD $in", "$out\n", "", 0, "arg $in = \"$out\"");
	testcmd("echo $in | $CMD", "$out\n", "", 0, "stdin $in = \"$out\"");
	testcmd("$CMD -- -$in", "-$out\n", "", 0, "arg -$in = \"-$out\"");
	testcmd("echo -$in | $CMD", "-$out\n", "", 0, "stdin -$in = \"-$out\"");
}

sub test_standard_options {
	diag('Testing -h (--help) option...');
	likecmd("$CMD -h",
	        '/  Show this help/i',
	        '/^$/',
	        0,
	        'Option -h prints help screen');

	diag('Testing -v (--verbose) option...');
	likecmd("$CMD -hv",
	        '/^\n\S+ \d+\.\d+\.\d+/s',
	        '/^$/',
	        0,
	        'Option -v with -h returns version number and help screen');

	diag('Testing --version option...');
	likecmd("$CMD --version",
	        '/^\S+ \d+\.\d+\.\d+/',
	        '/^$/',
	        0,
	        'Option --version returns version number');
	return;
}

sub testcmd {
	my ($Cmd, $Exp_stdout, $Exp_stderr, $Exp_retval, $Desc) = @_;
	defined($descriptions{$Desc}) &&
		BAIL_OUT("testcmd(): '$Desc' description is used twice");
	$descriptions{$Desc} = 1;
	my $stderr_cmd = '';
	my $cmd_outp_str = $Opt{'verbose'} >= 1 ? "\"$Cmd\" - " : '';
	my $Txt = join('', $cmd_outp_str, defined($Desc) ? $Desc : '');
	my $TMP_STDERR = "$CMDB-stderr.tmp";
	my $retval = 1;

	if (defined($Exp_stderr)) {
		$stderr_cmd = " 2>$TMP_STDERR";
	}
	$retval &= is(`$Cmd$stderr_cmd`, $Exp_stdout, "$Txt (stdout)");
	my $ret_val = $?;
	if (defined($Exp_stderr)) {
		$retval &= is(file_data($TMP_STDERR),
		              $Exp_stderr, "$Txt (stderr)");
		unlink($TMP_STDERR);
	} else {
		diag("Warning: stderr not defined for '$Txt'");
	}
	$retval &= is($ret_val >> 8, $Exp_retval, "$Txt (retval)");

	return $retval;
}

sub likecmd {
	my ($Cmd, $Exp_stdout, $Exp_stderr, $Exp_retval, $Desc) = @_;
	defined($descriptions{$Desc}) &&
		BAIL_OUT("likecmd(): '$Desc' description is used twice");
	$descriptions{$Desc} = 1;
	my $stderr_cmd = '';
	my $cmd_outp_str = $Opt{'verbose'} >= 1 ? "\"$Cmd\" - " : '';
	my $Txt = join('', $cmd_outp_str, defined($Desc) ? $Desc : '');
	my $TMP_STDERR = "$CMDB-stderr.tmp";
	my $retval = 1;

	if (defined($Exp_stderr)) {
		$stderr_cmd = " 2>$TMP_STDERR";
	}
	$retval &= like(`$Cmd$stderr_cmd`, $Exp_stdout, "$Txt (stdout)");
	my $ret_val = $?;
	if (defined($Exp_stderr)) {
		$retval &= like(file_data($TMP_STDERR),
		                $Exp_stderr, "$Txt (stderr)");
		unlink($TMP_STDERR);
	} else {
		diag("Warning: stderr not defined for '$Txt'");
	}
	$retval &= is($ret_val >> 8, $Exp_retval, "$Txt (retval)");

	return $retval;
}

sub file_data {
	# Return file content as a string
	my $File = shift;
	my $Txt;

	open(my $fp, '<', $File) or return undef;
	local $/ = undef;
	$Txt = <$fp>;
	close($fp);
	return $Txt;
}

sub create_file {
	# Create new file and fill it with data
	my ($file, $text) = @_;
	my $retval = 0;

	open(my $fp, ">$file") or return 0;
	print($fp $text);
	close($fp);
	$retval = is(file_data($file), $text,
	             "$file was successfully created");

	return $retval; # 0 if error, 1 if ok
}

sub print_version {
	# Print program version
	print("$progname $VERSION\n");
	return;
}

sub usage {
	# Send the help message to stdout
	my $Retval = shift;

	if ($Opt{'verbose'}) {
		print("\n");
		print_version();
	}
	print(<<"END");

Usage: $progname [options]

Contains tests for the $CMDB(1) program.

Options:

  -a, --all
    Run all tests, also TODOs.
  -h, --help
    Show this help.
  -q, --quiet
    Be more quiet. Can be repeated to increase silence.
  -t, --todo
    Run only the TODO tests.
  -v, --verbose
    Increase level of verbosity. Can be repeated.
  --version
    Print version information.

END
	exit($Retval);
}

sub msg {
	# Print a status message to stderr based on verbosity level
	my ($verbose_level, $Txt) = @_;

	$verbose_level > $Opt{'verbose'} && return;
	print(STDERR "$progname: $Txt\n");
	return;
}

__END__

# This program is free software; you can redistribute it and/or modify it under 
# the terms of the GNU General Public License as published by the Free Software 
# Foundation; either version 2 of the License, or (at your option) any later 
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
# FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with 
# this program.
# If not, see L<http://www.gnu.org/licenses/>.

# vim: set ts=8 sw=8 sts=8 noet fo+=w tw=79 fenc=UTF-8 :
